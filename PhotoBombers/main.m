//
//  main.m
//  PhotoBombers
//
//  Created by Rob Randell on 28/04/2014.
//  Copyright (c) 2014 Rob Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RJRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RJRAppDelegate class]));
    }
}
