//
//  RJRPhotosViewController.h
//  PhotoBombers
//
//  Created by Rob Randell on 28/04/2014.
//  Copyright (c) 2014 Rob Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJRPhotosViewController : UICollectionViewController

@end
